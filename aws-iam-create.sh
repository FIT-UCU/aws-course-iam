#!/bin/bash

# Use:
# cat courseid_ABCD_participants.csv | ./aws-iam-create.sh > output.txt

# Requires:
# aws-cli jq openssl

# Define access here (or profile below)
#export AWS_ACCESS_KEY_ID=AKIAIOSFODNN7EXAMPLE
#export AWS_SECRET_ACCESS_KEY=wJalrXUtnFEMI/K7MDENG/bPxRfiCYEXAMPLEKEY
#export AWS_DEFAULT_REGION=us-west-2

# Define profile here (~/.aws/credentials) (or KEYS before)
export AWS_PROFILE=ucucloud
GROUP=ucustudents
CONSOLE="https://444445555555.signin.aws.amazon.com/console"

export AWS_PAGER=''
while read LINE; do

	if [[ "$LINE" =~ ^$ ]]; then 
		continue
	else 
		if [[ "$LINE" =~ Direcci ]]; then
			continue
		else
			DIR=$(echo $LINE | cut -d, -f3)
			NAME=$(echo $DIR | cut -d@ -f1)
			PASS=$(openssl rand -base64 12)
			echo $NAME - $DIR

			aws iam create-user \
				--user-name $NAME \
				--tags Key=Course,Value=$PROFILE > /dev/null 2>&1  

			OUTLOGIN=$(aws iam create-login-profile \
				--user-name $NAME \
				--password $PASS) 

			OUTAPI=$(aws iam create-access-key \
				--user-name $NAME)

			aws iam add-user-to-group \
				--group-name $GROUP \
				--user-name $NAME

			echo "========================="
			echo "CONSOLE Access: $CONSOLE"
			echo "Username: "$(echo $OUTLOGIN | jq .LoginProfile.UserName)
			echo "Password: $PASS"
			echo
			echo "API KEY: "$(echo $OUTAPI | jq .AccessKey.AccessKeyId)
			echo "API SECRET KEY: "$(echo $OUTAPI | jq .AccessKey.SecretAccessKey)
			echo "========================"
			echo
		fi
	fi
done


#aws iam list-users
exit 0
