# AWS course setup

This simple script receive the .csv file of Webasignatura export and create a user into IAM with console and API access.

## Setup

### 1. Obtain .cvs file from Webasignatura course participants. 

Delete the other professors from list

### 2. Create AWS account 

Create an organization account 

Create IAM group `ucustudents` and add `AdministratorAccess` policy to the group.

Create the first API user into IAM & configure access `aws` command.

### 3. Configure script

```
export AWS_PROFILE=ucucloud
GROUP=ucustudents
CONSOLE="https://444445555555.signin.aws.amazon.com/console" 
```

## Execution

The execution receives the .csv file from stdin and output the list of access as they may be send to each student.

```
cat courseid_ABCD_participants.csv | ./aws-iam-create.sh > output.txt
```

## Output

```
juan.pueblo - juan.pueblo@correo.ucu.edu.uy
=========================
CONSOLE Access: https://444445555555.signin.aws.amazon.com/console
Username: "juan.pueblo"
Password: zWh5FC+vVMgV3/
API KEY: "AKIAWO6YENMSC6RETZ"
API SECRET KEY: "3nmVC685oy8GWS8I6kTcnkn4ul+7tMPef/42b"
========================

rodrigo.diaz - rodrigo.diaz@correo.ucu.edu.uy
=========================
CONSOLE Access: https://444445555555.signin.aws.amazon.com/console
Username: "rodrigo.diaz"
Password: pEe7Tm37ImwgPz
API KEY: "AKIM6YENJYPDMASFVT"
API SECRET KEY: "85okdRlVmx8562RpI1/clX5+TzVSQP3qwMpcHv"
========================
```

## License

MIT License
